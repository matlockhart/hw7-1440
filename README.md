# HW7: Recursive Web Crawler with cURLpp

Follow the instructions given on the assignment page to get your Cloud9 workspace ready for this assignment

https://usu.instructure.com/courses/474707/assignments/2295301

When your workspace is ready, build the programs in the examples/ directory to test that everything is in order.  You should study these programs to learn how to use the cURLpp and regex libraries. Please feel free to change, break and fix these examples to your heart's content. They are there to help you get started.


Here are links to some documents that may come in handy as you work on this assignment:

https://curl.haxx.se/libcurl/c/curl_easy_setopt.html
http://www.cplusplus.com/reference/regex/match_results/


Good luck!

-- Erik
