#include <iostream>
#include <map>
#include <regex>
#include <iterator>
#include <sstream>
#include <vector>
#include <string>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>

std::map<std::string, std::string> modifyMap(std::string k, std::string v, std::map<std::string, std::string> b) 
{
    b[k] = v;
    return b;
}

bool urlHasDomain(std::string url)
{
    std::size_t found = url.find("http");
    if(found == std::string::npos)
        return false;
    else return true;
}

std::string extractDomain(std::string url) 
{
    std::regex re("(http.*?//[^/]+.*[$/])");        //need expression that will find domain under all circumstances  DONE
    std::smatch m;

    std::string ret("");

    if (std::regex_search(url, m, re)) {
        ret = m[0].str();
    }
    //std::cout << "I found '" << ret << "'\n"; // DELETE ME
    return ret;
}

std::vector<std::string> getUrls(std::string html, std::string domain)
{
    std::vector<std::string> urls;
    try
    {
        std::regex re("<a .*?href=\"((?:(https?:\/\/)([^\/\"#]+)|\/)([^\"]*))\".*?>");        /* make sure expression works in brackets DONE -> used \  */
        
        std::sregex_iterator iter(html.begin(), html.end(), re);
        std::sregex_iterator end;
        
        while(iter != end)
        {
            if(urlHasDomain((*iter)[1]))
                urls.push_back((*iter)[1]);
            else
            {
                std::string tempStr;
                tempStr += domain; 
                tempStr += (*iter)[1];
                urls.push_back(tempStr);
            }
            ++iter;
        }
        
    }
    catch(std::regex_error& e)
    {
        std::cout << e.what() << std::endl;
    }
    
    return urls;
}

void urlDiver(std::string url, int maxDepth, std::map<std::string, std::string> & map, int depth = 0)
{
    std::string html, currentDomain = extractDomain(url);
    std::vector<std::string> urls;
    
    try             //puts all html into string  DONE
    {
        curlpp::Cleanup cleaner;            
        curlpp::Easy request;               
        
        curlpp::options::Url curlUrl(url);
        request.setOpt(curlUrl);            
        
        curlpp::options::FollowLocation follow(true);
        request.setOpt(follow);             
        
        std::ostringstream os;
        os << request << std::endl;         
        
        html = os.str();                     
        //std::cout << html << std::endl;     
    }
    catch ( curlpp::LogicError & e ) {
        std::cout << e.what() << std::endl;
    }
    catch ( curlpp::RuntimeError & e ) {
        std::cout << e.what() << std::endl;
    }
    
    urls = getUrls(html, currentDomain);       //urls in html are found and put into vector of strings  DONE
    
    for(int i = 0; i < urls.size(); i++) //iterates as many times as the # of urls found  DONE
    {
        std::string currentUrl;
        currentUrl = urls[i];
        
        std::size_t found1 = currentUrl.find("#"), found2 = currentUrl.find("mailto:");
        if(found1 == std::string::npos && found2 == std::string::npos)                 //need if statement for hyperlinks beginning with #  DONE
        {
            auto mapSearch = map.find(currentUrl);
            if(mapSearch == map.end())           //check if url has been listed before  DONE
            {
                for(int i = depth; i > 0; i--)
                {
                    std::cout << "                ";
                }
                
                if(urlHasDomain(currentUrl))            //needs to figure out if domain is there or needs to be added   DONE
                    std::cout << currentUrl << std::endl;  
                else
                    std::cout << currentDomain << currentUrl << std::endl;
                
                
                map.insert(std::make_pair(currentUrl, "here"));
                
                if(depth < maxDepth)        //call function again recursively for next with updated depth DONE
                    urlDiver(currentUrl, maxDepth, map, depth + 1);
            }
        }
        
    }
}

int main(int argc, char** argv) {
    
    std::string there;
    int maxDepth;
    
    std::cout << "Welcome to Mat Lockhart's URL Crawler!!!" << std::endl << "Enter a URL to crawl: ";
    std::cin >> there;
    std::cout << "Enter a depth to crawl to (1-4): ";
    std::cin >> maxDepth;
    maxDepth = maxDepth - 1;
    while(maxDepth == 0 || maxDepth > 3)
    {
        std::cout << "ERROR!" << std::endl << "Please re-enter a depth to crawl to (1-4)";
        std::cin >> maxDepth;
        maxDepth = maxDepth - 1;
    }
    
    std::map<std::string, std::string> map;

    if (argc > 1)
        there = argv[1];

    if (argc > 2)
        maxDepth = atoi(argv[2]);

    std::cout << "Recursively crawling the web to a depth of " << maxDepth + 1 << " links beginning from " << there << std::endl;
    
    urlDiver(there, maxDepth, map);
    //  _____ ___  ___   ___  
    // |_   _/ _ \|   \ / _ (_)
    //   | || (_) | |) | (_) |
    //   |_| \___/|___/ \___(_)
    //                        
    // 0. Define a recursive function that will, given a URL and a maximum      D
    //    depth, follow all hyperlinks found until the maximum depth is
    //    reached. You may define a wrapper function to make calling this
    //    recursive function from main() easier.
    //
    // 1. Your recursive function will create a new curlpp::Easy object each    D
    //    time it's called.  it should also clean up after itself.
    //
    // 2. Your recursive function needs to account for relative URLs. For
    //    example, the address of Don Knuth's FAQ page is
    //    http://www-cs-faculty.stanford.edu/~knuth/faq.html. If you inspect
    //    the contents of the page with your browser's developer tools
    //    (Ctrl-Shift-I) or view the source (Ctrl-U), you'll see that many of
    //    the links therein do not begin with
    //    http://www-cs-faculty.stanford.edu/.
    //
    //    Your recursive function needs to account for this by remembering what
    //    the current domain name is, and being prepared to prepend that to the
    //    URL it parses out of any given hyperlink.
    //
    // 3. Your recursive function should skip hyperlinks beginning with # -
    //    they refer to locations on the same page.
    //
    // 4. Your recursive function must also keep track of all pages it's
    //    visited so that it doesn't waste time visiting the same one again and
    //    again. This is where the std::map comes in handy. Your recursive
    //    function takes it as an argument, and returns it, possibly modified,
    //    after each invocation.


    return 0;
}
